# @pushrocks/smartmoney
handle monetary values

## Availabililty and Links
* [npmjs.org (npm package)](https://www.npmjs.com/package/@pushrocks/smartmoney)
* [gitlab.com (source)](https://gitlab.com/pushrocks/smartmoney)
* [github.com (source mirror)](https://github.com/pushrocks/smartmoney)
* [docs (typedoc)](https://pushrocks.gitlab.io/smartmoney/)

## Status for master
[![build status](https://gitlab.com/pushrocks/smartmoney/badges/master/build.svg)](https://gitlab.com/pushrocks/smartmoney/commits/master)
[![coverage report](https://gitlab.com/pushrocks/smartmoney/badges/master/coverage.svg)](https://gitlab.com/pushrocks/smartmoney/commits/master)
[![npm downloads per month](https://img.shields.io/npm/dm/@pushrocks/smartmoney.svg)](https://www.npmjs.com/package/@pushrocks/smartmoney)
[![Known Vulnerabilities](https://snyk.io/test/npm/@pushrocks/smartmoney/badge.svg)](https://snyk.io/test/npm/@pushrocks/smartmoney)
[![TypeScript](https://img.shields.io/badge/TypeScript->=%203.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![node](https://img.shields.io/badge/node->=%2010.x.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![JavaScript Style Guide](https://img.shields.io/badge/code%20style-prettier-ff69b4.svg)](https://prettier.io/)

## Usage

For further information read the linked docs at the top of this readme.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy.html)

[![repo-footer](https://pushrocks.gitlab.io/assets/repo-footer.svg)](https://maintainedby.lossless.com)
