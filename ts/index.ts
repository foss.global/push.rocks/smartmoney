import * as plugins from './smartmoney.plugins';

export const parseEuropeanNumberString = (europeanNumberString: string): number => {
  return parseFloat(europeanNumberString.replace('.', '').replace(',', '.'));
};
