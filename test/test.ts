// tslint:disable-next-line: no-implicit-dependencies
import { expect, tap } from '@pushrocks/tapbundle';
import * as smartmoney from '../ts/index';

tap.test('first test', async () => {
  const myNumber = smartmoney.parseEuropeanNumberString('1.000,50');
  expect(myNumber).to.equal(1000.5);
});

tap.start();
